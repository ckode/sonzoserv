#   Copyright 2013 David Brown - dcbrown73 - at - yahoo - . - com
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# ##########################################################################
from sonzoserv.telnet import TelnetServer, TelnetProtocol
import time

LMAGENTA = chr(27) + "[1;35m"
WHITE    = chr(27) + "[37m"
LGREEN   = chr(27) + "[1;32m"
LOGIN    = "\n\r\n\r\n\r                             {}Welcome to Sonzo Chat!\n\r\n\r{}"


class ChatClient(TelnetProtocol):
    """
    Custom server side client object inherited from sonzo.SonzoClient.
    
    This client IS NOT an actual client side telnet client.
    """
    def _init_(self, sock, addr):
        # Overridden method
        self.name = addr
        
    def onConnect(self):
        # Overridden method    
        for user in USERLIST:
            systemMessage(user, "{} has joined the chat!\n\r".format(self.getAddrPort()))
        USERLIST.append(self)        
        systemMessage(self, LOGIN.format(LMAGENTA, WHITE))
    
    
    def onDisconnect(self):
        # Over-ridden method 
        USERLIST.remove(self)
        for user in USERLIST:
            if user is not self:
                systemMessage(user, "{} logged off.\n\r".format(self.getAddrPort()))


    def dataRecieved(self, data):
        """
        Overriden function.  Called when data is recieved from client.
        """
        chat(self, data)
        

    def disconnectUser(self):
        """
        Disconnect user.
        """
        systemMessage(self, "Goodbye!\n\r")
        self.disconnect()


def color(c, color):
    if c._ansi:
        return color
    else:
        return ""
        

def chat(client, msg):
    # Check to see if someone issues a command.
	
	# Enable or disable ANSI color.
    if msg.startswith("=a".lower()):
        client.setANSIMode()
        systemMessage(client, "ANSI: {}\n\r".format(client.inANSIMode()))
        return
		
	# Disconnect
    if msg.startswith("/quit".lower()):
        client.disconnectUser()
        return

    # Enable / disable Character Mode / Line Mode		
    if msg.startswith("~".lower()):
        client.setCharacterMode()
        systemMessage(client, "Character Mode is now: {}\n\r".format(client.getCharacterMode()))
        return
	
    # Setup a function to call later()	
    if msg.startswith("/runlater".lower()):
        chatsrvr.callLater("Ran 2 seconds later.", func=print, delay=2)   
		
	# Install a function into the main loop
    if msg.startswith("/install".lower()):
        chatsrvr.install("Installed Function!", func=print)
		
	# Shut the server down	
    if msg.startswith("/shutdown".lower()):
        chatsrvr.shutdown()
        
    # If no command, say it in the chat room.
    for c in USERLIST:
        sendMessage(c, msg)

  
def sendMessage(client, message):
    message = "{}{} says, {}{}".format(color(client, LGREEN), client.getAddrPort(), color(client, WHITE), message)
    client.send(message)

def systemMessage(client, message):
    client.send(message)
    
    
    
if __name__ == '__main__':
    USERLIST = []
    chatclient = ChatClient
    chatsrvr = TelnetServer(client=chatclient, address='', port=23)
    
    # Create and start a looping call.
    tensecondloop = chatsrvr.loopingCall("Looping at 10 seconds", func=print)
    tensecondloop.start(10)
    
    # Start the main loop. 
    chatsrvr.run()