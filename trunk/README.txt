SonzoServ - A cross-platform Python 3 based Asynchronous Telnet Application Framework

SonzoServ (or sonzoserv) is a Python 3 framework for creating asynchronous telnet server based 
applications.  The heart of sonzoserv (the telnet server) is based on the work of Jim Storch when 
he created the miniboa asynchronous telnet server.  The SonzoServ framework has extensively employed
much of his hard work.  

Credit must also be given to Mark Richardson (omegus) who added code to allow the telnet server to 
actively sense if a client supports ANSI colors. 

SonzoServ is called a framework because it has a few non-telnet related features.  Like miniboa,
SonzoServ's original intent was to create a Multi-User Dungeon.(MUD) MUDs don't exist on Telnet 
alone.

Features:
- Asynchronous operation.  
- Telnet protocol negotiation happens before a client is returned to the application removing the need for your application to do it.
- Has a Main Application Loop
- The ability to install your own functions into the Main Application Loop.
- The ability to schedule looping calls to functions at specific time interval in seconds.
- The ability to schedule a functions to call later after specific amount of time in seconds passes.
- Runs on both Windows and Linux. (has not tested on any other platforms)

SonzoServ is licensed under the Apache 2 License.

